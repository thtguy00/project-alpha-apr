Feature 1
    step 1: python -m venv .venv
    step 2: .\.venv\Scripts\Activate.ps1
    step 3: python -m pip install --upgrade pip
    step 4: pip install django
    step 5: pip install black
    step 6: pip install flake8
    step 7: pip install djhtml
    step 8: deactivate
    step 9: .\.venv\Scripts\Activate.ps1
    step 10: python -m pip freeze > requirements.txt
    step 11: migrate and test
    step 12: git add .  

Feature 2
    step 1: django-admin startproject tracker .
    step 2: python manage.py startapp accounts, projects, tasks
    step 3: register apps in Django project at the top of installed_apps
    step 4: run migration
    step 5: create super user
    step 6: test
    step 7: git add .

Feature 3
    step 1: create class Project in models.py in projects app
    step 2: add name attribute with models.Charfield
    step 3: add description attribute with models.Textfield
    step 4: add member attribute with manytomanyField
            import setting and auth.user model
    step 5: use __str__ method to return the name as the name of user
    step 6: migrate, test, add

Feature 4
    step 1: register model in admin 
    step 2: test and add 

Feature 5
    step 1: create class view for projectListView
    step 2: create template folder within projects
    step 3: create projects folder within template
    step 4: create list.html within templates/projects
    step 5: create url.py file within projects
    step 6: within url.py
            import path
            create urlpatter
            register url path
    step 7: import include in tracker url
            include projects.urls 
    step 8: complete html requirement within list.html

Feature 6
    step 1: import RedirectView, reverse_lazy
    step 2: add path under urlpattern with 

Feature 7 
    step 1: create LoginView in accounts
    step 2: create url path in urls.py
    step 3: create template folder and html file
    step 4: create form inside html file
    step 5: add " LOGIN_REDIRECT_URL inside tracker setting.py

Feature 8
    step 1: add LoginRequiredMixin to list view in project
    step 2: change queryset to members

Feature 9
    step 1: import logout view in account url
    step 2: register path under url pattern
    step 3: add LOGOUT_REDIRECT_URL in tracker settings.py

Feature 10:
    step 1: create function view under accounts/views
    step 2: create if else statement
    step 3: use method="post"
    step 4: use form.is_valid
    step 5: use UserCreationForm (import)
    step 6: create signup.html under registation folder
    step 7: create form using csrf token and form.as_p
    step 8: create button

Feature 11:
    step 1: create task model 
    step 2: USER_MODEL = settings.AUTH_USER_MODEL
    step 3: use def __str__(self)

Feature 12:
    step 1: register task model in admin

Feature 13
    step 1: create class view for detail in projects
    step 2: get_queryset(self)
    step 3: register path 
    step 4: create template
    step 5: use if statements and for statements to create table 
    step 6: add {{ project.tasks.all|length }} to show number of tasks in list
    step 7: add link before project name

Feature 14
    step 1: add CreateView in project views
    step 2: LoginRequiredMixin
    step 3: register path in project.urls
    step 4: create template with form=method"post"
    step 5: add link in list view that direct to create view

Feature 15
    step 1: create TaskCreateView
    step 2: LoginRequiredMixin
    step 3: add link to redirect to detail.html
    step 4: register path
    step 5: create template

Feature 16
    step 1: Create TaskListView
    step 2: LoginRequiredMixin
    step 3: register path
    step 4: in list.html, use if else statement and include table with links
    
Feature 17
    step 1: create update view without a template
    step 2: import reverse_lazy
    step 3: add success_url = reverse_lazy("show_my_tasks")
    step 4: add onto list.html for task
    step 5: add task.is_completed|yesno:"Done, "
    
Feature 18
    step 1: pip install django-markdownify
    step 2: install in INSTALL_APPS
    step 3: add MARKDOWNIFY = {"default": {"BLEACH": False}} in tracker setting
    step 4: replace description with {{ project.description|markdownify }}

Feature 19
    step 1: add extra urls in base.html